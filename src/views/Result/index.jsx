import React, { Component } from 'react';
import axios from "axios";

import Header from "../../components/Header";

import "./css/style.scss";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: false,
      searchBar: "",
      isLoading: true,
      email: "",
      password: "",
      accessToken: "",
      uid:"",
      client:"",
      descricao: "",
      foto: ""
    }

    this.onClickNav = this.onClickNav.bind(this);
  }

  componentDidMount() {
    this.getEmpresa();
  }

  getEmpresa = async () => {
    this.setState({ isLoading: true });
    const accessToken = localStorage.getItem("accessToken");
    const client = localStorage.getItem("client");
    const uid = localStorage.getItem("uid");
    this.setState({ accessToken });
    this.setState({ client });
    this.setState({ uid });
    await axios.get(`http://empresas.ioasys.com.br/api/v1/enterprises?name=${this.props.match.params.id}`, {
       headers: { 
          'access-token': accessToken,
          'client': client,
          'uid': uid
       } }
    )
    .then(response => {
      this.setState({ descricao: response.data.enterprises[0].description });
      this.setState({ foto: response.data.enterprises[0].photo });
    })
    .catch(error => {
      console.log(error);
    })
    .finally(() => {
      this.setState({ isLoading: false });
    });
  }

  handleFormChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleSubmit = async (e) => {
    e.preventDefault(e);
    this.onClickNav();
    this.getEmpresas(this.state.searchBar);
  }

  onClickNav() {
    this.setState({ search: !this.state.search });
  }

  render() {
    return (
      <div>
        <Header onClickNav={this.onClickNav} valueSearch={this.state.searchBar} onChange={this.handleFormChange} onSubmit={this.handleSubmit} search={this.state.search} />
        <div id="result-view">
          <div className="section">
            <div className="container">
              <div className="row search-false">
                <div className="col s12 m12">
                  <div className="card">
                    <div className="row">
                      <div align="center" className="col s12 m12">
                        { this.state.foto === null ?
                          null
                         :
                          <img src={this.state.foto} alt="" />
                        }
                      </div>
                    </div>
                    <div className="row">
                      <div className="col s12 m12">
                        <div className="card-stacked">
                          <div className="card-content">
                            <p className="descricao">{this.state.descricao}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;