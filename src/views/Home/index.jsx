import React, { Component } from 'react';
import axios from "axios";

import Header from "../../components/Header";

import "./css/style.scss";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: false,
      searchBar: "",
      isLoading: true,
      isEmpresa: false,
      email: '',
      password: '',
      accessToken: "",
      uid:"",
      client:"",
      empresas: []
    }

    this.onClickNav = this.onClickNav.bind(this);
  }

  componentDidMount() {
    let accessToken = localStorage.getItem("accessToken");
    let client = localStorage.getItem("client");
    let uid = localStorage.getItem("uid");
    this.setState({ accessToken });
    this.setState({ client });
    this.setState({ uid });
  }

  getEmpresas = async (searchBar) => {
    this.setState({ isLoading: true });
    this.setState({ isEmpresa: false });
    const accessToken = this.state.accessToken;
    const client = this.state.client;
    const uid = this.state.uid;
    await axios.get(`http://empresas.ioasys.com.br/api/v1/enterprises?name=${searchBar}`, {
       headers: { 
          'access-token': accessToken,
          'client': client,
          'uid': uid
       } }
    )
    .then(response => {
      this.setState({ empresas: response.data.enterprises });
      console.log(response.data.enterprises);
    })
    .catch(error => {
      console.log(error);
    })
    .finally(() => {
      this.setState({ isLoading: false });
      this.setState({ isEmpresa: true });
    });
  }

  handleFormChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleSubmit = async (e) => {
    e.preventDefault(e);
    this.onClickNav();
    this.getEmpresas(this.state.searchBar);
  }

  onClickNav() {
    this.setState({ search: !this.state.search });
  }

  render() {
    return (
      <div>
        <Header onClickNav={this.onClickNav} valueSearch={this.state.searchBar} onChange={this.handleFormChange} onSubmit={this.handleSubmit} search={this.state.search} />
        <div data-testid="home-view" id="home-view">
          <div className="section">
            <div className="container">
              { this.state.searchBar !== "" ? (
                  this.state.isLoading ? (
                      <div className="row">
                        <div className="col s12 m12 l12 xl12">
                          <p className="is-loading-custom">Carregando...</p>
                        </div>
                      </div>
                  ) : this.state.isEmpresa ? (
                    <div className="row search-false">
                      {this.state.empresas.map((empresa, i) => (
                        <div className="col s12 m12" key={i}>
                          <a href={`/resultado/${empresa.enterprise_name}`} className="card horizontal">
                            <div className="col s12 m4">
                              { empresa.photo === null ?
                                null
                                :
                                <img src={empresa.photo} alt="" />
                              }
                            </div>
                            <div className="col s12 m8">
                              <div className="card-stacked">
                                <div className="card-content">
                                  <p className="nome-empresa">{empresa.enterprise_name}</p>
                                  <p className="negocio">{empresa.enterprise_type.enterprise_type_name}</p>
                                  <p className="cidade">{empresa.city}</p>
                                </div>
                              </div>
                            </div>
                          </a>
                        </div>
                      ))}
                    </div>
                  ) : (
                    <div className="search-false">
                      <p>Nenhum resultado encontrado.</p>
                    </div>
                  )
                ) : (
                  <div className="search-false">
                    <p>Clique na busca para iniciar.</p>
                  </div>
                )
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;