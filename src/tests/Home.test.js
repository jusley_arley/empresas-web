import React from "react";
import { render } from "@testing-library/react";
import Home from "../views/Home";

test('Id', () => {
  const { getByTestId } = render( <Home /> );
  expect(getByTestId('home-view'));
});

test('Texto na tela', async () => {
  const { findByText } = render( <Home /> );
  const item = await findByText('Clique na busca para iniciar.');
  expect(item);
});

test('Header tag', () => {
  const { container } = render(<Home />, {
    container: document.createElement(`Header`),
  });
  expect(container);
});
