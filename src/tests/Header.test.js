import React from "react";
//import ReactDOM from 'react-dom';
import { render } from "@testing-library/react";
import Header from "../components/Header";
//import App from '../App';

/*it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});*/

test("header create", () => {
  const { container } = render(<Header />, {
      container: document.createElement('header'),
    }
  );
  expect(container);
});

test("SearchBar create", () => {
  const { container } = render(<Header />, {
      container: document.createElement('SearchBar'),
    }
  );
  expect(container);
});

/*test('Placeholder', async () => {
  const { getByPlaceholderText } = render( <Header /> );
  const item = await getByPlaceholderText('Pesquisar');
  expect(item);
});*/
