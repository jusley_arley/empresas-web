import React from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";

import Home from "./views/Home";
import Login from "./views/Login";
import Result from "./views/Result";
import ScrollToTop from "./components/ScrollToTop";

import "./assets/css/index.scss";

const Routes = () => (
  <Router>
    <ScrollToTop>
      <main>
        <div className="main">
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/inicio" component={Home} />
            <Route exact path="/resultado/:id" component={Result} />
          </Switch>
        </div>
      </main>
    </ScrollToTop>
  </Router>
);

export default Routes;
